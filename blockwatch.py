#!/usr/bin/env python
from __future__ import print_function

from time import sleep
import subprocess
import sys
import urllib2

from lxml import html

URL = 'http://mmpool.bitparking.com/blockstats'
SLEEP = 2 * 60


class Block(object):
    def __init__(self, poolid, state):
        self.poolid = poolid
        self.state = state


def handle_block(blocks, silent, poolid, state, number, payment):
    if poolid not in blocks:
        blocks[poolid] = Block(poolid, state)
        if not silent:
            yield 'New block found: %s! (%s)' % (poolid, state)
    block = blocks[poolid]
    if state != block.state:
        block.state = state
        if not silent:
            if state == 'valid':
                smiley = ' :)'
            elif state == 'orphan':
                smiley = ' :('
            else:
                smiley = ''
            yield 'Block %s changed state to %s.%s' % (poolid, state, smiley)
            if number and payment:
                yield 'https://blockchain.info/block-height/%s paid %s' % (number, payment)


class Run(object):
    def __init__(self, base):
        self.base = base

    def __call__(self, msg):
        subprocess.check_call(self.base + [msg])


argv = sys.argv[1:]
silent = True
if '--showfirst' in argv:
    argv.remove('--showfirst')
    silent = False
if len(argv):
    cmd = Run(argv)
else:
    cmd = print


blocks = {}

while True:
    page = urllib2.urlopen(URL)
    tree = html.fromstring(page.read())
    for e in tree.xpath('//h1'):
        if e.text.strip() == 'Blocks':
            while e.tag != 'table':
                e = e.getnext()
            for tr in e.xpath('./tr'):
                if not tr.xpath('./td'):
                    continue
                poolid, number, time, duration, reward, payment, shares, state = \
                    [td.text.strip() for td in tr.xpath('./td')]
                poolid = int(poolid)
                try:
                    number = int(number)
                except ValueError:
                    number = None
                for msg in handle_block(blocks, silent, poolid, state, number, payment):
                    cmd(msg)
    # first run done, watch for changes now
    silent = False
    sleep(SLEEP)
