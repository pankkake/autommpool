A tool to automatically withdraw coins
from `mmpool <http://mmpool.bitparking.com>`_ if they go over
a preconfigured balance.

Only Python should be required, though versions below 2.7
should also have ``simplejson``.

Usage::

    python autommpool.py <config file>

By default, the config file is ``autommpool.ini``.

See ``autommpool.example.ini`` for how to configure it.

Quick setup::

    git clone https://bitbucket.org/pankkake/autommpool.git
    cd autommpool
    cp autommpool.example.ini autommpool.ini
    $EDITOR autommpool.ini
    python autommpool.py

Typical output::

    bitcoin: 0.02591817
    devcoin: 0.41926900
    ixcoin: 3.62301310
     Asking to widthdraw 3.62301310 ixcoins.
    namecoin: 0.00063138

There are two modes of operation. If the ``allbalances`` section is present,
it will only try to withdraw coins when the allbalance limit of any coin
is reached (as long as they also are over the ``balances`` limit).
That way, the coins should usually be withdrawn at the same time.

You can run it in a cron. When a withdraw occurs, it is on stderr,
so if you only want to be notified of such an event,
you can add to your crontab::

    # every hour at minute 42
    42 * * * * /home/myuser/autommpool.py /home/myuser/autommpool.ini > /dev/null

Even better, to withdraw as soon as possible, but still keep tabs once a day
on your balances::

    # every hour at minute 42
    42 * * * * /home/myuser/autommpool.py /home/myuser/autommpool.ini > /dev/null
    # every day
    0 0 * * * /home/myuser/autommpool.py /home/myuser/autommpool.ini


If you like this script, please tip 1Md5kvYempaW3NkYxdYbFezL6mKg7QwERe.
