#!/usr/bin/env python

import sys
import urllib2
from ConfigParser import ConfigParser, NoOptionError, NoSectionError
from decimal import Decimal
from urlparse import urljoin

try:
    import simplejson as json
except ImportError:
    import json

SERVER = 'http://mmpool.bitparking.com/'

confile = sys.argv[1] if len(sys.argv) > 1 else 'autommpool.ini'
with open(confile) as f:
    config = ConfigParser()
    config.readfp(f)

api = urllib2.urlopen(urljoin(SERVER, '/userstats/%s') % config.get('user', 'name'))
apitree = json.loads(api.read(), parse_float=Decimal)
apibals = {}
for bal in apitree['balances']:
    apibals[bal['coin']] = bal['balance']

enough = []

for cur, bal in apibals.iteritems():
    act = urljoin(SERVER, '/withdrawCoin/%s/%s' % (config.get('user', 'name'), cur))
    print "%s: %.8f" % (cur, bal)
    try:
        maxbal = config.get('balances', cur)
        if maxbal:
            maxbal = Decimal(maxbal)
            if bal > maxbal:
                enough.append((bal, cur, act))
    except (NoOptionError, NoSectionError):
        print " Warning: no trigger/minimal balance for %s" % cur

allmode = False
allok = False
for bal, cur, act in enough:
    try:
        allbal = config.get('allbalances', cur)
        if allbal:
            allbal = Decimal(allbal)
            allmode = True
            if bal > allbal:
                print >>sys.stderr, " Will try to withdraw everything because %s is over %s." % (cur, allbal)
                allok = True
    except (NoOptionError, NoSectionError):
        pass

# either in allmode and we have allok, or we are in normal mode
if allmode is allok:
    for bal, cur, act in enough:
        print >>sys.stderr, " Asking to withdraw %s %ss." % (bal, cur)
        urllib2.urlopen(urljoin(SERVER, act), data='')
